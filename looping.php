<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        /* 
            Soal No 1 
            Looping I Love PHP
            Lakukan Perulangan (boleh for/while/do while) sebanyak 20 iterasi. Looping terbagi menjadi dua: Looping yang pertama Ascending (meningkat) 
            dan Looping yang ke dua menurun (Descending). 

            Output: 
            LOOPING PERTAMA
            2 - I Love PHP
            4 - I Love PHP
            6 - I Love PHP
            8 - I Love PHP
            10 - I Love PHP
            12 - I Love PHP
            14 - I Love PHP
            16 - I Love PHP
            18 - I Love PHP
            20- I Love PHP
            LOOPING KEDUA
            20 - I Love PHP
            18 - I Love PHP
            16 - I Love PHP
            14 - I Love PHP
            12 - I Love PHP
            10 - I Love PHP
            8 - I Love PHP
            6 - I Love PHP
            4 - I Love PHP
            2 - I Love PHP
        */
        // Lakukan Looping Di Sini
        $x = 0;
        $jawabanku = "";
        while ($x<21) {
            // KODE DIBAWAH UNTUK LOOPING PERTAMA
            if($x === 0){
                $jawabanku .= "LOOPING PERTAMA";
                $x++;
            }
            if ($x%2 != 0){
                $x++;
            }
            if($x%2 == 0){
                $jawabanku .= "<br>" . $x . ' - I Love PHP';
                $x++;
            }


            //KODE DIBAWAH UNTUK LOOPING KEDUA
            if($x === 21){
                $jawabanku .= '<br>LOOPING KEDUA';
                $y = 20;
                while( $y > 1 ){
                    if($y%2 === 0){
                        $jawabanku .= "<br>" . $y . ' - I Love PHP';
                        $y--;
                    }else{
                        $y--;
                    }
                }
            }
        }
        echo "$jawabanku <br>";

        echo "<h3>Soal No 2 Looping Array Modulo </h3>";
        /* 
            Soal No 2
            Looping Array Module
            Carilah sisa bagi dengan angka 5 dari setiap angka pada array berikut.
            Tampung ke dalam array baru bernama $rest 
        */

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        // Lakukan Looping di sini
        echo "<br><br><b>Array diatas jika dibagi dengan 5 sisa baginya : </b><br>";
        $indexArray = 0;
        foreach ($numbers as $number) {
            echo "<br>";
            echo "Array ke $indexArray yaitu $number sisa baginya adalah: " . ($number%5); 
            echo "<br>";
            $indexArray++;
        }


        echo "<h3> Soal No 3 Looping Asociative Array </h3>";
        /* 
            Soal No 3
            Loop Associative Array
            Terdapat data items dalam bentuk array dimensi. Buatlah data tersebut ke dalam bentuk Array Asosiatif. 
            Setiap item memiliki key yaitu : id, name, price, description, source. 
            
            Output: 
            Array ( [id] => 001 [name] => Keyboard Logitek [price] => 60000 [description] => Keyboard yang mantap untuk kantoran [source] => logitek.jpeg ) 
            Array ( [id] => 002 [name] => Keyboard MSI [price] => 300000 [description] => Keyboard gaming MSI mekanik [source] => msi.jpeg ) 
            Array ( [id] => 003 [name] => Mouse Genius [price] => 50000 [description] => Mouse Genius biar lebih pinter [source] => genius.jpeg ) 
            Array ( [id] => 004 [name] => Mouse Jerry [price] => 30000 [description] => Mouse yang disukai kucing [source] => jerry.jpeg ) 

            Jangan ubah variabel $items

        */
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        //MENAMBAHKAN KEY DI VARIABEL ITEMS
        $tambahKey = [];
        $arrayDenganKey = [];
        $z = 0;
        foreach ($items as $item) {
            $tambahKey[$z]["id"] = $item[0];
            $tambahKey[$z]["name"] = $item[1];
            $tambahKey[$z]["price"] = $item[2];
            $tambahKey[$z]["description"] = $item[3];
            $tambahKey[$z]["source"] = $item[4];
            $z++;
        }
        //MENAMPILKAN VARIABEL ITEMS YANG SUDAH DIBERI KEY
        // Output: 
        foreach ($tambahKey as $key) {
            print_r($key);
            echo "<br>";
        }
        
        echo "<h3>Soal No 4 Asterix </h3>";
        /* 
            Soal No 4
            Asterix 5x5
            Tampilkan dengan looping dan echo agar menghasilkan kumpulan bintang dengan pola seperti berikut: 
            Output: 
            * 
            * * 
            * * * 
            * * * * 
            * * * * *
        */

        // //Kurang Efektif;
        // $hasil = "";
        // $bintang = "* ";
        // for ($m=0; $m<5 ; $m++) { 
        //     $hasil .= $bintang;
        //     $bintang .= "* ";
        //     $hasil .= "<br>";
        // }
        // echo "Asterix: <br>$hasil";
        // echo "<br>";      
        
        
        //Efektif
        for ($a = 1 ; $a <= 5 ; $a++){
            for ($b = 1 ; $b <= $a ; $b++){
                echo "* ";
            }
            echo "<br>";
        }
    ?>

</body>
</html>